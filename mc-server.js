const http = require("http");
const child = require("child_process");
const fs = require('fs');

http.createServer((req, res) => {
    // pull and send back
    
    if(req.url.indexOf("getLatest") != -1) {
        try{
            let file = '';
            child.exec('git pull origin master', (err, std, stderr) => {
                if(err || stderr) throw new Error(err || stderr);
                console.log(std);
    
                child.exec('zip -r "latest_serv.zip "./"', (err1, std1, stderr1) => {
                    if(err1 || stderr1) throw new Error(err1 || stderr1);
                    console.log(std1);

                    let rs = fs.createReadStream("latest_serv.zip");
                    rs.on('data', chunk => {
                        console.log(`Getting ${chunk.length} bytes of data.`);
                        file += chunk.toString();
                    })
                    rs.on('end', () => {
                        console.log("\ndone\nsending file to client");
                        res.end(file);
                    })
                });
            });
        } catch (e) {
            res.statusCode = 503;
            res.end(JSON.stringify(e, null, 4));
        }
        
    }
}).listen(25566);


